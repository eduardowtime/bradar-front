import { Component, OnInit } from '@angular/core';
import { AdminLayoutSections } from '../../layouts/admin-layout/admin-layout.routing'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  constructor() { }

  ngOnInit() {
    this.menuItems = AdminLayoutSections
  }

}
