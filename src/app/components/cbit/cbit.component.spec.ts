import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CbitComponent } from './cbit.component';

describe('CbitComponent', () => {
  let component: CbitComponent;
  let fixture: ComponentFixture<CbitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CbitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
