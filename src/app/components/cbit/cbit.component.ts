import { Component, OnInit, Input } from '@angular/core';
import { BitComponent } from '../../layouts/bit/bit.component'

@Component({
  selector: 'app-cbit',
  templateUrl: './cbit.component.html',
  styleUrls: ['./cbit.component.scss']
})
export class CbitComponent implements OnInit {

  constructor(private bit: BitComponent) { }
  @Input() data: any[]
  ngOnInit() {
    document.onclick = function(e){
      if(window.event.srcElement.classList.contains('title')) {
        var t = window.event.srcElement.closest('.accordion') 
        var x = window.event.srcElement.closest('.title') 
        if(t.classList.contains('is-active')) {
          t.classList.remove('is-active');
          x.classList.remove('icon-down');
          x.classList.add('icon.right')
        } else {
          t.classList.add('is-active');
          x.classList.add('icon-down');
          x.classList.remove('icon.right')
        }
      }
			//console.clear();
		}
  }

  compare(value1, symbol, value2 ) {
    if(symbol == '<') {
      return value1 < value2;
    } else {
      return value1 >= value2;
    }
  }

}
