import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CbitComponent } from './cbit/cbit.component';

@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    CbitComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
