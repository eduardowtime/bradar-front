import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { LoginModel } from '../../models/login-model'
import { HttpService } from '../../_services/http-service.service';
import { Router } from '@angular/router';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  LoginModel = new LoginModel('', '')
  res: any;

  constructor(
    private HttpService: HttpService, 
    private router: Router,
    private auth: AuthenticationService
    ){ }

  ngOnInit() {
    if (this.auth.is_authenticated()) {
      this.router.navigate(['/'])
    }
  }

  public login(){
    this.HttpService.post('auth/login', this.LoginModel)
    .subscribe(
      data=>{
        if( data['success'] ) {
          this.auth.authenticate(data)
        } else {
          Swal.fire({
            title: 'Erro!',
            text: data['message'],
            type: 'error' 
          });
        }
      }
    );
  }

}
