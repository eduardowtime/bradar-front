import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-transmissao',
  templateUrl: './transmissao.component.html',
  styleUrls: ['./transmissao.component.scss']
})
export class TransmissaoComponent implements OnInit {

  data;
  state: Boolean = true;
  constructor() { }

  ngOnInit() {
    this.data = moment().format('DD/MM/YYYY HH:mm:ss')
  }

  changeState() {
    this.data = moment().format('DD/MM/YYYY HH:mm:ss')
  }

}
