import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../_services/websocket.service';
import { BitService } from '../../_services/bit.service.service';

@Component({
  selector: 'app-bit',
  templateUrl: './bit.component.html',
  styleUrls: ['./bit.component.scss']
})
export class BitComponent implements OnInit {
  type: String = 's';
  state: Boolean;

  constructor(private socket: WebsocketService, private bit: BitService) { }

  ngOnInit() {
    this.socket.setService('bit');
    this.socket.listen('message').subscribe((data)=> {
      this.bit.data = data;
      this.bit.data = JSON.parse(this.bit.data)
    })
    
  }

  emit() {
    this.socket.send('sdf')
  }
}
