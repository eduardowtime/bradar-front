import { HttpClientModule } from '@angular/common/http';
import {NgModule} from '@angular/core'
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { TransmissaoComponent } from '../transmissao/transmissao.component'
import { DashboardComponent } from '../dashboard/dashboard.component';
import { BitComponent } from '../bit/bit.component';
import { LoginComponent } from '../login/login.component';
import { CbitComponent } from 'src/app/components/cbit/cbit.component';



@NgModule({
  declarations: [
    TransmissaoComponent,
    DashboardComponent,
    BitComponent,
    LoginComponent,
    CbitComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule
  ]
})
export class AdminLayoutModule { }
