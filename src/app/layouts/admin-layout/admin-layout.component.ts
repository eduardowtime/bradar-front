import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  logged: boolean = false;
  painel;
  constructor( private auth: AuthenticationService) { }

  ngOnInit() {
    this.painel = 1;
    this.auth.menuEmitter.subscribe(
      show => this.logged = show
    );
  }

}
