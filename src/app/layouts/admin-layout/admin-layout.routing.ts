import { Routes } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { TransmissaoComponent } from '../transmissao/transmissao.component'
import { DashboardComponent } from '../dashboard/dashboard.component';
import { BitComponent } from '../bit/bit.component';
import { LoginComponent } from '../login/login.component';
import { AuthGuardService } from '../../_services/auth.guard.service';


export const AdminLayoutRoutes: Routes = [
    { path: '', component: DashboardComponent, canActivate: [AuthGuardService]},
    { path: 'dashboard',    component: DashboardComponent, data: { routeName: 'Dashboard' }, canActivate: [AuthGuardService] },
    { path: 'transmissao',  component: TransmissaoComponent, data: { routeName: 'Habilitar/Desabilitar Transmissão' }, canActivate: [AuthGuardService] },
    { path: 'bit', component: BitComponent, data: { routeName: 'Bit' }, canActivate: [AuthGuardService] },
    { path: 'login', component: LoginComponent, data: {routeName: 'Login'} }

];

export const AdminLayoutSections = [
    { 
        "section": "painel", 
        "routes": [
            AdminLayoutRoutes[2],
            AdminLayoutRoutes[3]
        ]
    },
    {
        "section": "elemento",
        "routes": [
            AdminLayoutRoutes[1],
            AdminLayoutRoutes[4]
        ]
    }
]