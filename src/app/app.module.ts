import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CommonModule } from '@angular/common';
import { HttpService } from './_services/http-service.service';
import { AuthGuardService } from './_services/auth.guard.service';
import { AuthenticationService } from './_services/authentication.service';
import { BitService } from './_services/bit.service.service'

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    NavbarComponent,
    SidebarComponent
  ],
  imports: [
    RouterModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [
    HttpService,
    AuthenticationService,
    AuthGuardService,
    BitService
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
