import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../../environments/environment';

@Injectable()
export class HttpService {

  constructor(private http:HttpClient){}

  get(url:string): Observable<any> {
    return this.http.get(config.url+url)
  }

  post(url: string, params, options = {}) {
    return this.http.post(
      config.url+url,
      params,
      options
    );
  }


}