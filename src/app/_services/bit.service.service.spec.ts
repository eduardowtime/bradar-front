import { TestBed } from '@angular/core/testing';

import { Bit.ServiceService } from './bit.service.service';

describe('Bit.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Bit.ServiceService = TestBed.get(Bit.ServiceService);
    expect(service).toBeTruthy();
  });
});
