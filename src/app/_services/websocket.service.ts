import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private socket: any;
  private service: String;
  messages: any[];
  constructor() { }

  setService(service) {
    this.service = service;
  }

  listen(event:string) {
    this.socket = io.connect('ws://localhost:9999/'+this.service);
    return new Observable((subscriber) => {
      this.socket.on(event, (data) => {
        subscriber.next(data);
      })
    })
  }

  send(msg) {
      this.socket.send(msg)
  }
}
