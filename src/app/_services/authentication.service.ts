import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { config } from '../../environments/environment';
import { LoginModel } from '../models/login-model';
import { HttpClient } from 'selenium-webdriver/http';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class AuthenticationService {
    private authenticated = false;
    public permissions;
    public username;
    public token;

    menuEmitter= new EventEmitter<boolean>();
    

    constructor(private router: Router) {}


    authenticate(data) {
        if(data.success) {
            this.authenticated = true;
            this.permissions = data.data.permissions;
            this.token = data.data.token;
            this.username = data.data.username;
            this.menuEmitter.emit(true);
            this.router.navigate(['/']);
        } else {
            this.menuEmitter.emit(false);
        }
        
    }

    is_authenticated():boolean {
        return this.authenticated;
    }

    logout() {
        this.authenticated = false;
        this.menuEmitter.emit(false);
        this.router.navigate(['/login'])
    }
}